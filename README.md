# VidyoCore-Xamarin

Sample Xamarin app that integrates the VidyoCore library. 
This project was built and tested with Visual Studio 8.3.7 and VidyoCore.framework 0.45

## iOS Library Setup 

The following steps explain how to integrate *VidyoCore.framework* for iOS in your Xamarin iOS project:

1. Copy the client certificate ca-certificates-base.crt into your iOS project (e.g. into Resources group). This file is required for Vidyo licensing.
2. Select Entitlements.plist and add the following entitlements: 

```xml
<key>keychain-access-groups</key>
<array>
    <string>$(AppIdentifierPrefix)io.100grams.VidyoCoreApp</string>
    <string>$(AppIdentifierPrefix)VidyoLicense</string>
</array>
```

3. Select Info.plist and add the following keys:

```xml
<key>UIBackgroundModes</key>
<array>
    <string>audio</string>
    <string>voip</string>
</array>
<key>NSCameraUsageDescription</key>
<string>Camera is needed for video calls</string>
<key>NSMicrophoneUsageDescription</key>
<string>Microphone is needed for video calls</string>
```

4. Right-click the solution in the Solution navigator pane and select **Add > New Project**, then select **iOS > Library > Binding Library** and name it **VidyoCoreBinding**.
5. Right-click **Native References** in your *VidyoCoreBinding* project and select **Add Native Reference**, then select  VidyoCore.framework (provided separately).
6. Right-click **Packages** in your *VidyoCoreBinding* project and select *Manage NuGet Packages*, then add **Xamarin.Swift** package. VidyoCore.framework uses Swift 5.
6. Still in your VidyoCoreBinding project, edit your ApiDefinition.cs file to look like this: 

```cs
using System;

using ObjCRuntime;
using Foundation;
using UIKit;

namespace VidyoCore
{
    // @interface VidyoViewController : UIViewController <UIGestureRecognizerDelegate>
    [BaseType(typeof(UIViewController), Name = "_TtC9VidyoCore19VidyoViewController")]
    interface VidyoViewController
    {
        /// +(VidyoViewController * _Nonnull)createWithGuestLink:(NSURL * _Nonnull)guestLink onCallEnd:(void (^ _Nullable)(void))onCallEnd __attribute__((warn_unused_result));
        /// <summary>
        ///  The designated initializer for joining a Vidyo call as guest.
        ///  This methid instantiates a new VidyoViewController and returns it.
        /// </summary>
        /// <param name="guestLink">the `GuestLink` that was clicked by the user.</param>
        /// <param name="onCallEnd">a block to perform when the call ends (or fails)</param>
        [Static]
        [Export("createWithGuestLink:onCallEnd:")]
        VidyoViewController CreateWithGuestLink(NSUrl guestLink, [NullAllowed] Action onCallEnd);

        /// +(void)createAsyncWithAppId:(NSString * _Nonnull)appId callCode:(NSString * _Nonnull)callCode onCallEnd:(void (^ _Nullable)(void))onCallEnd completion:(void (^ _Nullable)(NSError * _Nullable, VidyoViewController * _Nullable))completion;
        /// <summary>
        /// Creates a new `VidyoViewController` asynchronously, after validating the appId and call code.
        /// </summary>
        /// <param name="appId">the unique application Id, required for accessing the Vidyo API</param>
        /// <param name="callCode">a unique String to identify the resource / call.</param>
        /// <param name="onCallEnd">a block to perform when the call ends (or fails)</param>
        /// <param name="completion">a block returning the initialized contorller and an `Error`, if occurred.</param>                         
        [Static]
        [Export("createAsyncWithAppId:callCode:onCallEnd:completion:")]
        void CreateAsyncWithAppId(string appId, string callCode, [NullAllowed] Action onCallEnd, [NullAllowed] Action<NSError, VidyoViewController> completion);
    }

}
```

7. VidyoCore.framework exports no structs/enums so your **Structs.cs** should not change except that, for consistency reasons, you should use the same namespace in Struct.cs as in ApiDefinition.cs. Here is an empty Structs.cs file: 

```cs
using System;

namespace VidyoCore
{
}
```

8. In the Solution pane, click the iOS project and select **Project > Edit References...** from the top menu. In the "Edit References" window, select the **Projects** pane and then select VidyoCoreBinding to add it as a reference. 

9. To use the VidyoCore.framework in your code, refer to **ViewController.cs** in this project. Here is an excerpt for joining a call with a Facetalk callId: 

```cs
Button.TouchUpInside += delegate
{
    VidyoViewController.CreateAsyncWithAppId("685", "26063", () =>
    {
        Console.WriteLine("Call ended.");
        NavigationController.PopToRootViewController(true);

    }, (error, vc) =>
    {
        if (error != null)
        {
            Console.WriteLine("Error joining call: " + error);
        }
        else if (vc != null)
        {
            NavigationController.PushViewController(vc, true);
        }
    });
};
```

10. Build the solution (all projects). 

**Note: VidyoCore.framework supports only *arm* architecture, i.e. your app can be debugged on the device only and not on the simulator.**



## Android library Setup:

The steps below are based on the [Xamarin docs](https://docs.microsoft.com/en-us/xamarin/android/platform/binding-java-library/binding-an-aar).

1. Create a new Java Bindings Library project:
Right-click the solution in the Solution navigator pane and select **Add > New Project**, then select **Android > Library > Bindings Library** and name it **VidyoCoreBindingAndroid**.

2. Add the VidyoCore .AAR file to the project: 
Right-click **Jars** in your *VidyoCoreBindingAndroid* project and select **Add Files...**, then select  the VidyoCore.aar (provided separately).

3. Set the appropriate build action for the .AAR file:
Right click the .aar file and select **Build Action > LibraryProjectZip**

4. Build the VidyoCoreBindingAndroid project.

5. Add a reference to the VidyoCoreBindingAndroid from your Xamarin Android app (Droid project):
Select the Droid project in your solution and then select **Project > Edit References...** in the top menu.
Tick the box next to VidyoCoreBindingAndroid and click OK. 

6. In order to use VidyoCore in your project, your Xamarin Application object must extend **VidyoCore**, and you must also declare an Activity that extends **InCallActivity** which you will start using **NewVidyoIntent**.

Here is sample code for using VidyoCore in your Xamarin Android project:

```cs
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using System;

using Com.Vidyo.Vidyocore;
using Com.Vidyo.Vidyocore.Interfaces;
using Com.Vidyo.Vidyocore.Activities;

namespace VidyoCoreApp.Droid
{
    [Application]
    public class MainApp : VidyoCore
    {
        public MainApp(IntPtr javaReference, Android.Runtime.JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
        }
    }

    [Activity(Label = "VidyoCoreTest", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity, IOKHTTPResponse
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.myButton);

            button.Click += delegate {
                VidyoCore.NewVidyoIntent(this, 685, 26063, this);
            };
        }

        void IOKHTTPResponse.OnResponse(Intent p0)
        {
            // TODO
            Console.WriteLine("Joined call!");

            var intent = new Intent(this, typeof(VidyoCallActivity));
            intent.PutExtras(p0.Extras);
            StartActivity(intent);
        }

        void IOKHTTPResponse.OnFailure(String error)
        {
            // TODO
            Console.WriteLine("Call initiation failed. Error: " + error);
        }

    }

    [Activity]
    public class VidyoCallActivity : InCallActivity
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Console.WriteLine("InCallActivity OnCreate");
        }
    }
}

```
