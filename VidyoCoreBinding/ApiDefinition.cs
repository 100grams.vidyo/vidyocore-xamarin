﻿using System;

using ObjCRuntime;
using Foundation;
using UIKit;

namespace VidyoCore
{
    // @interface VidyoViewController : UIViewController <UIGestureRecognizerDelegate>
    [BaseType(typeof(UIViewController), Name = "_TtC9VidyoCore19VidyoViewController")]
    interface VidyoViewController
    {
        /// +(VidyoViewController * _Nonnull)createWithGuestLink:(NSURL * _Nonnull)guestLink onCallEnd:(void (^ _Nullable)(void))onCallEnd __attribute__((warn_unused_result));
        /// <summary>
        ///  The designated initializer for joining a Vidyo call as guest.
        ///  This methid instantiates a new VidyoViewController and returns it.
        /// </summary>
        /// <param name="guestLink">the `GuestLink` that was clicked by the user.</param>
        /// <param name="onCallEnd">a block to perform when the call ends (or fails)</param>
        [Static]
        [Export("createWithGuestLink:onCallEnd:")]
        VidyoViewController CreateWithGuestLink(NSUrl guestLink, [NullAllowed] Action onCallEnd);

        /// +(void)createAsyncWithAppId:(NSString * _Nonnull)appId callCode:(NSString * _Nonnull)callCode onCallEnd:(void (^ _Nullable)(void))onCallEnd completion:(void (^ _Nullable)(NSError * _Nullable, VidyoViewController * _Nullable))completion;
        /// <summary>
        /// Creates a new `VidyoViewController` asynchronously, after validating the appId and call code.
        /// </summary>
        /// <param name="appId">the unique application Id, required for accessing the Vidyo API</param>
        /// <param name="callCode">a unique String to identify the resource / call.</param>
        /// <param name="onCallEnd">a block to perform when the call ends (or fails)</param>
        /// <param name="completion">a block returning the initialized contorller and an `Error`, if occurred.</param>                         
        [Static]
        [Export("createAsyncWithAppId:callCode:onCallEnd:completion:")]
        void CreateAsyncWithAppId(string appId, string callCode, [NullAllowed] Action onCallEnd, [NullAllowed] Action<NSError, VidyoViewController> completion);
    }

}

