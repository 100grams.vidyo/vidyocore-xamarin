﻿using Android.App;
using Android.Widget;
using Android.OS;
using Com.Vidyo.Vidyocore;
using Com.Vidyo.Vidyocore.Interfaces;
using Com.Vidyo.Vidyocore.Activities;
using Android.Content;
using System;

namespace VidyoCoreApp.Droid
{
    [Application]
    public class MainApp : VidyoCore
    {
        public MainApp(IntPtr javaReference, Android.Runtime.JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();
        }
    }

    [Activity(Label = "VidyoCoreTest", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity, IOKHTTPResponse
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.myButton);

            button.Click += delegate {
                VidyoCore.NewVidyoIntent(this, 685, 26063, this);
            };
        }

        void IOKHTTPResponse.OnResponse(Intent p0)
        {
            // TODO
            Console.WriteLine("Joined call!");

            var intent = new Intent(this, typeof(VidyoCallActivity));
            intent.PutExtras(p0.Extras);
            StartActivity(intent);
        }

        void IOKHTTPResponse.OnFailure(String error)
        {
            // TODO
            Console.WriteLine("Call initiation failed. Error: " + error);
        }

    }

    [Activity]
    public class VidyoCallActivity : InCallActivity
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Console.WriteLine("InCallActivity OnCreate");
        }
    }
}

